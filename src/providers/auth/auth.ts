import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

import {Storage} from "@ionic/storage";

@Injectable()
export class AuthProvider {

  isAuthentificated: boolean = true;

  constructor(public http: Http, private storage: Storage) {

    this.storage.set('name', 'Oleg')

    this.storage.get('name').then(data => {
      console.log(data, '...');
    })
    .catch(error => {
      console.log(error);
    })

  }

  logout() {
    console.log('logout triggered');
  }

  authStateChanged() {
    let ur = "http://loanwebsite.app/dummydata"
    // let ur = "https://jsonplaceholder.typicode.com/posts/"
    return this.http.get(ur)
  }

}
