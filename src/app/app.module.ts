import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';
import {IonicStorageModule} from "@ionic/storage";

import {MyApp} from './app.component';

// Providers
import {AuthProvider} from '../providers/auth/auth';

// Page
import {HomePage} from '../pages/home/home';
import {UsersPage} from "../pages/users/users";
import {UserPage} from "../pages/user/user";
import {TaskPage} from "../pages/task/task";
import {TasksPage} from "../pages/tasks/tasks";
import {HttpModule} from "@angular/http";
import {SigninPage} from "../pages/signin/signin";
import {SignupPage} from "../pages/signup/signup";

@NgModule({
  declarations: [
    MyApp,

    // Pages
    HomePage,
    UserPage,
    UsersPage,
    SigninPage,
    SignupPage,
    TaskPage,
    TasksPage,

  ],
  imports: [
    HttpModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,

    // Pages
    HomePage,
    UserPage,
    UsersPage,
    SigninPage,
    SignupPage,
    TaskPage,
    TasksPage,

  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider
  ]
})
export class AppModule {
}
