import {Component, ViewChild} from '@angular/core';
import {MenuController, NavController, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {HomePage} from '../pages/home/home';
import {AuthProvider} from "../providers/auth/auth";
import {UserPage} from "../pages/user/user";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = HomePage;
  homePage: any = HomePage;
  userPage: any = UserPage;


  @ViewChild('nav') nav: NavController

  constructor(platform: Platform,
              statusBar: StatusBar,
              splashScreen: SplashScreen,
              private menuCtrl: MenuController,
              private authProvider: AuthProvider) {


    this.authProvider.authStateChanged()
      .subscribe(data => {
        console.log(data);
      })

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }


  // SideMenu
  onLoad(page: any) {
    this.nav.setRoot(page);
    this.menuCtrl.close();
  }

  onLogout() {
    this.authProvider.logout()
    this.nav.setRoot(this.homePage)
    this.menuCtrl.close()
  }

  onMyAccount() {
    this.nav.setRoot(this.userPage)
    this.menuCtrl.close()
  }

}

